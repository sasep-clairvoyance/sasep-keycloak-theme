<#import "template.ftl" as layout>
<@layout.mainLayout active='sessions' bodyClass='sessions'; section>

    <#if section = "header">
        ${msg("sessionsHtmlTitle")}
    <#elseif section = "content">
        <table class="highlight">
            <thead>
            <tr>
                <td>${msg("ip")}</td>
                <td>${msg("started")}</td>
                <td>${msg("lastAccess")}</td>
                <td>${msg("expires")}</td>
                <td>${msg("clients")}</td>
            </tr>
            </thead>

            <tbody>
            <#list sessions.sessions as session>
                <tr>
                    <td>${session.ipAddress}</td>
                    <td>${session.started?datetime}</td>
                    <td>${session.lastAccess?datetime}</td>
                    <td>${session.expires?datetime}</td>
                    <td>
                        <#list session.clients as client>
                            ${client}<br/>
                        </#list>
                    </td>
                </tr>
            </#list>
            </tbody>

        </table>

    <#elseif section = "footer">
        <span>&nbsp;</span>

        <form action="${url.sessionsUrl}" method="post" class="hide" id="logout-all-sessions-form">
            <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">
            <button id="logout-all-sessions" class="hide">${msg("doLogOutAllSessions")}</button>
        </form>

        <a class="right text-darken-3 red-text"
           href="javascript:document.getElementById('logout-all-sessions-form').submit()">${msg("doLogOutAllSessions")}</a>
    </#if>
</@layout.mainLayout>
