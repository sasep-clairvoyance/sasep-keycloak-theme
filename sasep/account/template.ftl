<#macro mainLayout active bodyClass>
    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="robots" content="noindex, nofollow">

        <title>${msg("accountManagementTitle")}</title>
        <link rel="icon" href="${url.resourcesPath}/img/favicon.ico">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <#--        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>-->

        <#if properties.styles?has_content>
            <#list properties.styles?split(' ') as style>
                <link href="${url.resourcesPath}/${style}" rel="stylesheet"/>
            </#list>
        </#if>
        <#if properties.scripts?has_content>
            <#list properties.scripts?split(' ') as script>
                <script type="text/javascript" src="${url.resourcesPath}/${script}"></script>
            </#list>
        </#if>
    </head>
    <body class="admin-console user ${bodyClass}">

    <nav class="nav-extended light-green darken-3">
        <div class="nav-wrapper">
            <ul id="nav-mobile" class="left" style="height: 64px">
                <li><img style="width: 48px; height: 48px; margin: 8px 12px 0 16px; background: white; border-radius: 24px"
                         src="${url.resourcesPath}/img/clairvoyance.png" alt="clairvoyance"/>
                </li>
            </ul>
            <a href="#" class="brand-logo"
               style="text-transform: uppercase; font-size: 24px">clairvoyance</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <#if referrer?has_content && referrer.url?has_content>
                    <li><a href="${referrer.url}" id="referrer">Go Back</a></li>
                </#if>
                <li><a href="${url.logoutUrl}">${msg("doSignOut")}</a></li>
            </ul>
        </div>
        <div class="nav-content">
            <ul class="tabs" id="tabs">
                <li class="tab">
                    <a class="<#if active=='account'>active</#if>" href="${url.accountUrl}">${msg("account")}</a>
                </li>
                <#if features.passwordUpdateSupported>
                    <li class="tab">
                        <a class="<#if active=='password'>active</#if>" href="${url.passwordUrl}">${msg("password")}</a>
                    </li>
                </#if>
                <li class="tab">
                    <a class="<#if active=='totp'>active</#if>" href="${url.totpUrl}">${msg("authenticator")}</a>
                </li>
                <#if features.identityFederation>
                    <li class="tab">
                        <a class="<#if active=='social'>active</#if>"
                           href="${url.socialUrl}">${msg("federatedIdentity")}</a>
                    </li>
                </#if>
                <li class="tab">
                    <a class="<#if active=='sessions'>active</#if>" href="${url.sessionsUrl}">${msg("sessions")}</a>
                </li>
                <li class="tab">
                    <a class="<#if active=='applications'>active</#if>"
                       href="${url.applicationsUrl}">${msg("applications")}</a>
                </li>
                <#if features.log>
                    <li class="tab">
                        <a class="<#if active=='log'>active</#if>" href="${url.logUrl}">${msg("log")}</a>
                    </li>
                </#if>
                <#if realm.userManagedAccessAllowed && features.authorization>
                    <li class="tab">
                        <a class="<#if active=='authorization'>active</#if>"
                           href="${url.resourceUrl}">${msg("myResources")}</a>
                    </li>
                </#if>
            </ul>
        </div>
    </nav>

    <div class="row" style="margin-top: 20px">
        <div class="col m10 offset-m1 s12">
            <div class="card">
                <div class="card-content">
                    <span class="card-title"><#nested "header"></span>

                    <#if message?has_content>
                        <#if message.type = 'success'>
                            <div class="card-panel green white-text alert">
                                <i class="material-icons">check</i>
                                <span>${kcSanitize(message.summary)?no_esc}</span>
                            </div>
                        </#if>
                        <#if message.type = 'error'>
                            <div class="card-panel red white-text alert">
                                <i class="material-icons">error_outline</i>
                                <span>${kcSanitize(message.summary)?no_esc}</span>
                            </div>
                        </#if>
                    </#if>

                    <#nested "content">
                </div>
                <#if bodyClass!="applications">
                    <div class="card-action">
                        <#nested "footer">
                    </div>
                </#if>
            </div>
        </div>
    </div>

    <script>
    var instance = M.Tabs.init(document.getElementById("tabs"), {});
    </script>
    </body>
    </html>
</#macro>