<#import "template.ftl" as layout>
<@layout.mainLayout active='password' bodyClass='password'; section>

    <#if section = "header">
        ${msg("changePasswordHtmlTitle")}
    <#elseif section = "content">
        <form class="col s12" action="${url.passwordUrl}" method="post" id="change-password">

            <input type="text" id="username" name="username" value="${(account.username!'')}" autocomplete="username"
                   readonly="readonly" style="display:none;">

            <#if password.passwordSet>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" name="password" type="password" class="validate" autofocus
                               autocomplete="current-password">
                        <label for="password">${msg("password")}</label>
                    </div>
                </div>
            </#if>

            <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

            <div class="row">
                <div class="input-field col s12">
                    <input id="password-new" name="password-new" type="password" class="validate"
                           autocomplete="new-password">
                    <label for="password-new">${msg("passwordNew")}</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input id="password-confirm" name="password-confirm" type="password" class="validate"
                           autocomplete="new-password">
                    <label for="password-confirm">${msg("passwordConfirm")}</label>
                </div>
            </div>

            <button type="submit" class="hide" name="submitAction" value="Save">${msg("doSave")}</button>
        </form>

        <p class="grey-text right-align">${msg("allFieldsRequired")}</p>

    <#elseif section = "footer">
        <span>&nbsp;</span>
        <a class="right text-darken-3 light-green-text"
           href="javascript:document.getElementById('change-password').submit()">${msg("doSave")}</a>
    </#if>

</@layout.mainLayout>
