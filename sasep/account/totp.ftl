<#import "template.ftl" as layout>
<@layout.mainLayout active='totp' bodyClass='totp'; section>

    <#if section = "header">
        ${msg("authenticatorTitle")}
    <#elseif section = "content">

        <#if totp.enabled>
            <table class="striped">
                <thead>
                <#if totp.otpCredentials?size gt 1>
                    <tr>
                        <th colspan="4">${msg("configureAuthenticators")}</th>
                    </tr>
                <#else>
                    <tr>
                        <th colspan="3">${msg("configureAuthenticators")}</th>
                    </tr>
                </#if>
                </thead>
                <tbody>
                <#list totp.otpCredentials as credential>
                    <tr>
                        <td class="provider">${msg("mobile")}</td>
                        <#if totp.otpCredentials?size gt 1>
                            <td class="provider">${credential.id}</td>
                        </#if>
                        <td class="provider">${credential.userLabel!}</td>
                        <td class="action">
                            <form action="${url.totpUrl}" method="post" class="form-inline">
                                <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">
                                <input type="hidden" id="submitAction" name="submitAction" value="Delete">
                                <input type="hidden" id="credentialId" name="credentialId" value="${credential.id}">
                                <button id="remove-mobile" class="btn btn-default">
                                    <i class="pficon pficon-delete"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        <#else>

            <hr/>

            <ol>
                <li>
                    <p>${msg("totpStep1")}</p>

                    <ul>
                        <#list totp.policy.supportedApplications as app>
                            <li>${app}</li>
                        </#list>
                    </ul>
                </li>

                <#if mode?? && mode = "manual">
                    <li>
                        <p>${msg("totpManualStep2")}</p>
                        <p><span id="kc-totp-secret-key">${totp.totpSecretEncoded}</span></p>
                        <p><a href="${totp.qrUrl}" id="mode-barcode">${msg("totpScanBarcode")}</a></p>
                    </li>
                    <li>
                        <p>${msg("totpManualStep3")}</p>
                        <ul>
                            <li id="kc-totp-type">${msg("totpType")}: ${msg("totp." + totp.policy.type)}</li>
                            <li id="kc-totp-algorithm">${msg("totpAlgorithm")}: ${totp.policy.getAlgorithmKey()}</li>
                            <li id="kc-totp-digits">${msg("totpDigits")}: ${totp.policy.digits}</li>
                            <#if totp.policy.type = "totp">
                                <li id="kc-totp-period">${msg("totpInterval")}: ${totp.policy.period}</li>
                            <#elseif totp.policy.type = "hotp">
                                <li id="kc-totp-counter">${msg("totpCounter")}: ${totp.policy.initialCounter}</li>
                            </#if>
                        </ul>
                    </li>
                <#else>
                    <li>
                        <p>${msg("totpStep2")}</p>
                        <p><img src="data:image/png;base64, ${totp.totpSecretQrCode}" alt="Figure: Barcode"></p>
                        <p><a href="${totp.manualUrl}" id="mode-manual">${msg("totpUnableToScan")}</a></p>
                    </li>
                </#if>
                <li>
                    <p>${msg("totpStep3")}</p>
                    <p>${msg("totpStep3DeviceName")}</p>
                </li>
            </ol>

            <hr/>

            <form action="${url.totpUrl}" class="form-horizontal" method="post" id="addTOTP">
                <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

                <div class="row">
                    <div class="input-field col s12">
                        <input id="totp" name="totp" type="text" class="validate" autocomplete="off">
                        <label for="totp">${msg("authenticatorCode")} *</label>
                    </div>
                </div>
                <input type="hidden" id="totpSecret" name="totpSecret" value="${totp.totpSecret}"/>

                <div class="row">
                    <div class="input-field col s12">
                        <input id="userLabel" name="userLabel" type="text"
                               class="validate ${messagesPerField.printIfExists('userLabel','invalid')}"
                               autocomplete="off">
                        <label for="userLabel">${msg("totpDeviceName")} <#if totp.otpCredentials?size gte 1>*</#if></label>
                    </div>
                </div>

                <button type="submit" class="hide" id="saveTOTPBtn" name="submitAction" value="Save">${msg("doSave")}
                </button>
                <button type="submit" class="hide" id="cancelTOTPBtn" name="submitAction"
                        value="Cancel">${msg("doCancel")}
                </button>
            </form>
            <#if totp.otpCredentials?size == 0>
                <p class="right-align grey-text">* ${msg("requiredFields")}</p>
            </#if>
        </#if>
    <#elseif section = "footer">
        <#if url.referrerURI??>
            <a class="grey-text" href="${url.referrerURI}">${msg("backToApplication")?no_esc}</a>
        </#if>
        <span>&nbsp;</span>
        <a class="right text-darken-3 red-text"
           href="javascript:document.getElementById('cancelTOTPBtn').click()">${msg("doCancel")}</a>
        <a class="right text-darken-3 light-green-text"
           href="javascript:document.getElementById('saveTOTPBtn').click()">${msg("doSave")}</a>
    </#if>

</@layout.mainLayout>
