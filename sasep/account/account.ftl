<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <#if section = "header">
        ${msg("editAccountHtmlTitle")}
    <#elseif section = "content">
        <form class="col s12" action="${url.accountUrl}" method="post" id="account">

            <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

            <#if !realm.registrationEmailAsUsername>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="username" name="username" type="text"
                               class="validate ${messagesPerField.printIfExists('username','invalid')}"
                               value="${(account.username!'')}"
                               <#if !realm.editUsernameAllowed>disabled="disabled"</#if>>
                        <label for="username">${msg("username")} <#if realm.editUsernameAllowed>*</#if></label>
                    </div>
                </div>
            </#if>

            <div class="row">
                <div class="input-field col s12">
                    <input id="email" name="email" type="text"
                           class="validate ${messagesPerField.printIfExists('email','invalid')}"
                           value="${(account.email!'')}">
                    <label for="email">${msg("email")} *</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input id="firstName" name="firstName" type="text"
                           class="validate ${messagesPerField.printIfExists('firstName','invalid')}"
                           value="${(account.firstName!'')}">
                    <label for="firstName">${msg("firstName")} *</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="lastName" name="lastName" type="text"
                           class="validate ${messagesPerField.printIfExists('lastName','invalid')}"
                           value="${(account.lastName!'')}">
                    <label for="lastName">${msg("lastName")} *</label>
                </div>
            </div>

            <button type="submit" class="hide" name="submitAction" value="Save"
                    id="submitAccount">${msg("doSave")}</button>
        </form>

        <p class="right-align grey-text">* ${msg("requiredFields")}</p>
    <#elseif section = "footer">
        <#if url.referrerURI??>
            <a class="grey-text" href="${url.referrerURI}">${msg("backToApplication")?no_esc}</a>
        </#if>
        <span>&nbsp;</span>
        <a class="right text-darken-3 red-text"
           href="javascript:cancel()">${msg("doCancel")}</a>
        <a class="right text-darken-3 light-green-text"
           href="javascript:save()">${msg("doSave")}</a>

        <script>
        let submit = document.getElementById("submitAccount");

        function save() {
          submit.value = "Save";
          submit.click();
        }

        function cancel() {
          submit.value = "Cancel";
          submit.click();
        }

        </script>
    </#if>

</@layout.mainLayout>
