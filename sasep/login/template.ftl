<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayRequiredFields=false displayWide=false showAnotherWayIfPresent=true hideFooter=false>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="robots" content="noindex, nofollow">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <#if properties.meta?has_content>
            <#list properties.meta?split(' ') as meta>
                <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
            </#list>
        </#if>
        <title>${msg("loginTitle",(realm.displayName!''))}</title>
        <link rel="icon" href="${url.resourcesPath}/img/favicon.ico"/>
        <#if properties.styles?has_content>
            <#list properties.styles?split(' ') as style>
                <link href="${url.resourcesPath}/${style}" rel="stylesheet"/>
            </#list>
        </#if>
        <#if properties.scripts?has_content>
            <#list properties.scripts?split(' ') as script>
                <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
            </#list>
        </#if>
        <#if scripts??>
            <#list scripts as script>
                <script src="${script}" type="text/javascript"></script>
            </#list>
        </#if>
    </head>

    <body class="${properties.kcBodyClass!}">

    <nav>
        <div class="nav-wrapper light-green darken-3">
            <ul id="nav-mobile" class="left">
                <li><img style="width: 48px; height: 48px; margin: 8px 12px 0 16px; background: white; border-radius: 24px"
                         src="${url.resourcesPath}/img/clairvoyance.png" alt="clairvoyance"/>
                </li>
            </ul>
            <a href="#" class="brand-logo"
               style="text-transform: uppercase; font-size: 24px">
                ${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc}
            </a>
            <ul id="nav-mobile" class="right">
                <li><a href="javascript:history.back()"><i class="material-icons">cancel</i></a></li>
            </ul>
        </div>
    </nav>

    <div class="valign-wrapper" style="height: calc(100vh - 64px);">
        <div class="row" style="width: 100vw;">
            <div class="col m4 offset-m4 s12">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title"><#nested "header"></span>

                        <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
                            <#if message.type = 'success'>
                                <div class="card-panel green white-text alert">
                                    <i class="material-icons">check</i>
                                    <span>${kcSanitize(message.summary)?no_esc}</span>
                                </div>
                            </#if>
                            <#if message.type = 'warning'>
                                <div class="card-panel orange white-text alert">
                                    <i class="material-icons">warning</i>
                                    <span>${kcSanitize(message.summary)?no_esc}</span>
                                </div>
                            </#if>
                            <#if message.type = 'error'>
                                <div class="card-panel red white-text alert">
                                    <i class="material-icons">error_outline</i>
                                    <span>${kcSanitize(message.summary)?no_esc}</span>
                                </div>
                            </#if>
                            <#if message.type = 'info'>
                                <div class="card-panel light-blue white-text alert">
                                    <i class="material-icons">info</i>
                                    <span>${kcSanitize(message.summary)?no_esc}</span>
                                </div>
                            </#if>
                        </#if>

                        <#nested "form">

                        <#if displayInfo>
                            <#nested "info">
                        </#if>
                    </div>
                    <#if !hideFooter>
                        <div class="card-action">
                            <#nested "submit">
                        </div>
                    </#if>
                </div>
            </div>
        </div>
    </div>

    </body>
    </html>
</#macro>
