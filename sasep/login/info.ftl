<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "header">
        <#if messageHeader??>
            ${messageHeader}
        <#else>
            ${message.summary}
        </#if>
    <#elseif section = "form">
        <div id="kc-info-message">
            <div class="card-panel light-blue white-text alert">
                <i class="material-icons">info</i>
                <span>
                    ${message.summary}
                    <#if requiredActions??>
                        <#list requiredActions>:
                            <b><#items as reqActionItem>${msg("requiredAction.${reqActionItem}")}<#sep>, </#items></b></#list>
                    </#if>
                </span>
            </div>
        </div>
    <#elseif section="submit">
        <#if skipLink??>
        <#else>
            <#if pageRedirectUri?has_content>
                <a class="grey-text" href="${pageRedirectUri}">${kcSanitize(msg("backToApplication"))?no_esc}</a>
            <#elseif actionUri?has_content>
                <a class="light-green-text text-darken-3" href="${actionUri}">${kcSanitize(msg("proceedWithAction"))?no_esc}</a>
            <#elseif (client.baseUrl)?has_content>
                <a class="grey-text" href="${client.baseUrl}">${kcSanitize(msg("backToApplication"))?no_esc}</a>
            </#if>
        </#if>
    </#if>
</@layout.registrationLayout>