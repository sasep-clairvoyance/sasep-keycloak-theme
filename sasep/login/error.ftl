<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section >
    <#if section = "header">
        ${msg("errorTitle")}
    <#elseif section = "form">
        <div class="card-panel red white-text alert">
            <i class="material-icons">error_outline</i>
            <span>${message.summary?no_esc}</span>
        </div>

    <#elseif section="submit">
        <#if client?? && client.baseUrl?has_content>
            <p><a id="backToApplication" href="${client.baseUrl}">${kcSanitize(msg("backToApplication"))?no_esc}</a></p>
        </#if>
    </#if>
</@layout.registrationLayout>