<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>
    <#if section = "header">
        ${msg("doLogIn")}
    <#elseif section = "form">
        <div>
            <form class="col s12" onsubmit="login.disabled = true; return true;" action="${url.loginAction}"
                  method="post" id="login">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="username" name="username" type="text" class="validate" autofocus autocomplete="off">
                        <label for="username"><#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if></label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" name="password" type="password" class="validate">
                        <label for="password">${msg("password")}</label>
                    </div>
                </div>

                <#if realm.rememberMe && !usernameEditDisabled??>
                    <p>
                        <label for="rememberMe">
                            <input id="rememberMe" name="rememberMe" type="checkbox"
                                   <#if login.rememberMe??>checked="checked"</#if>/>
                            <span>${msg("rememberMe")}</span>
                        </label>
                    </p>
                </#if>

                <#if realm.resetPasswordAllowed>
                    <p class="right-align"><a href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></p>
                </#if>

                <input type="hidden" id="id-hidden-input" name="credentialId"
                       <#if auth.selectedCredential?has_content>value="${auth.selectedCredential}"</#if>/>

                <input class="hide" name="login" id="kc-login" type="submit" value="${msg("doLogIn")}"/>
            </form>
        </div>
        <p style="font-size: 1px">&nbsp;</p>
    <#elseif section = "submit">
        <#if realm.password && realm.registrationAllowed && !registrationDisabled??>
            <a class="grey-text" href="${url.registrationUrl}">${msg("doRegister")}</a>
        </#if>
        <a class="right text-darken-3 light-green-text"
           href="javascript:document.getElementById('login').submit()">${msg("doLogIn")}</a>
    <#elseif section = "info" >

    </#if>

</@layout.registrationLayout>
