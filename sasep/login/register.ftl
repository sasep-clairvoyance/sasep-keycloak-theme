<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "header">
        ${msg("registerTitle")}
    <#elseif section = "form">
        <div>
            <form class="col s12" action="${url.registrationAction}"
                  method="post" id="register">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="firstName" name="firstName" type="text"
                               class="validate ${messagesPerField.printIfExists('firstName','invalid')}"
                               value="${(register.formData.firstName!'')}" autofocus autocomplete="off">
                        <label for="firstName">${msg("firstName")}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="lastName" name="lastName" type="text"
                               class="validate ${messagesPerField.printIfExists('lastName','invalid')}"
                               value="${(register.formData.lastName!'')}">
                        <label for="lastName">${msg("lastName")}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" name="email" type="text"
                               class="validate ${messagesPerField.printIfExists('email','invalid')}"
                               value="${(register.formData.email!'')}">
                        <label for="email">${msg("email")}</label>
                    </div>
                </div>
                <#if !realm.registrationEmailAsUsername>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="username" name="username" type="text"
                                   class="validate ${messagesPerField.printIfExists('username','invalid')}"
                                   value="${(register.formData.username!'')}">
                            <label for="username">${msg("username")}</label>
                        </div>
                    </div>
                </#if>
                <#if passwordRequired??>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="password" name="password" type="password"
                                   class="validate ${messagesPerField.printIfExists('password','invalid')} ${messagesPerField.printIfExists('password-confirm','invalid')}"
                                   autocomplete="new-password">
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="password-confirm" name="password-confirm" type="password"
                                   class="validate ${messagesPerField.printIfExists('password-confirm','invalid')}">
                            <label for="password-confirm">Password</label>
                        </div>
                    </div>
                </#if>

                <input class="hide" name="login" id="kc-login" type="submit" value="${msg("doRegister")}"/>
            </form>
        </div>
        <p style="font-size: 1px">&nbsp;</p>
    <#elseif section = "submit">
        <a class="grey-text" href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a>
        <a class="right text-darken-3 light-green-text"
           href="javascript:document.getElementById('register').submit()">${msg("doRegister")}</a>

    </#if>
</@layout.registrationLayout>
